# nsduiskin

#### 介绍
nsDuiSkin是NSIS的一个安装包UI插件，结合NSIS和DuiLib，用于制作界面美观的安装程序。

#### 使用说明

在使用本插件制作NSIS安装包时需要以下两件事情：

> 1. 设计好相应的UI界面并配置好DuiLib资源文件。
> 2. 在NSIS脚本中调用nsDuiSkin.dll提供的相关接口完成安装或卸载的业务逻辑。

在插件的test目录内有全套的打包脚本和示例资源文件，参照例子进行定制即可。

##### 示例文件说明

FilesToInstall 该目录内存放要打包的app文件

source 该目录内存放了全部的NSIS脚本和DuiLib资源文件

build-setup.bat 用于生成安装程序，生成好的安装程序会放到output目录内

makeapp.bat 用于生成待安装的app文件的7z压缩包

makeskinzip.bat 用于生成DuiLib资源文件压缩包skin.zip

##### DuiLib资源文件说明

install.xml  安装程序总脚本
configpage.xml  安装程序配置界面
installingpage.xml  安装中界面
finishpage.xml 安装完成界面
uninstall.xml 卸载程序总脚本
uninstallpage.xml 卸载配置界面
uninstallingpage.xml 卸载中界面
uninstallfinishpage.xml 卸载完成界面
default.xml 滚动条配置文件
licensepage.xml 许可协议展示界面资源文件
msgbox.xml 弹出框资源文件

> **在编辑DuiLib资源文件(xml)时，可以使用[DuilibPreview](https://gitee.com/huixch/duilibpreview)对正在编辑的xml文件进行预览，该预览工具与nsDuiSkin使用的是相同版本的DuiLIb库。**

#### nsDuiSkin接口说明

##### 1. InitInstallPage

>  安装界面初始化接口，此接口函数用于初始化nsDuiSkin.dll插件的配置信息。

###### 调用示例

`nsDuiSkin::InitInstallPage "$PLUGINSDIR\" "${INSTALL_LICENCE_FILENAME}"`

###### 参数说明

| 参数序号 | 参数类型 | 参数说明                       | 备注                                                         |
| -------- | -------- | ------------------------------ | :----------------------------------------------------------- |
| 1        | 字符串   | 用于指定NSIS安装包插件释放路径 | 此路径的指定非常在重要，在脚本中指定的插件及UI资源包将会释放到此目录内。只有正确指定后，nsDuiSkin插件才能正确调用资源并显示窗口。 |
| 2        | 字符串   | 许可协议文件                   | 这是一个TXT或RTF文档，在nsDuiSkin插件加载时，将会加载此文件来显示许可协议。 |

###### 返回值

返回安装界面窗口句柄

##### 2. InitUnInstallPage

> 卸载界面初始化接口，此接口函数用于初始化nsDuiSkin.dll插件的配置信息。

###### 调用示例

`nsDuiSkin::InitUnInstallPage "$PLUGINSDIR\"` 

参数说明

| 参数序号 | 参数类型 | 参数说明 | 备注 |
| -------- | -------- | ------------------------------ | ------------------------------------------------------------ |
| 1        | 字符串   | 用于指定NSIS安装包插件释放路径 | 此路径的指定非常在重要，在脚本中指定的插件及UI资源包将会释放到此目录内。只有正确指定后，nsDuiSkin插件才能正确调用资源并显示窗口。 |

###### 返回值

返回卸载界面窗口句柄

##### 3. SetWindowTitle

> 设置安装程序窗口标题（同时也会将logo.ico图标设置为窗口图标）。

###### 调用示例

```
nsDuiSkin::SetWindowTile $hInstallDlg "${PRODUCT_NAME}安装程序"

nsDuiSkin::SetWindowTile $hInstallDlg "${PRODUCT_NAME}卸载程序" 
```

###### 参数说明

| 参数序号 | 参数类型 | 参数说明             | 备注                                                         |
| -------- | -------- | -------------------- | ------------------------------------------------------------ |
| 1        | 整型     | 要设置标题的窗口句柄 | 调用InitInstallPage、InitUnInstallPage以及InitMessageBox接口时返回的句柄。 |
| 2        | 字符串   | 窗口标题             |                                                              |

###### 返回值

无

##### 4. ShowTabPageItem

> 设置当前显示的TAB页，nsDuiSkin插件使用DuiLib中的TabLayout控件实现不同阶段的页面切换。

###### 调用示例

` nsDuiSkin::ShowTabPageItem $hInstallDlg "wizardTab" ${INSTALL_PAGE_CONFIG} `

###### 参数说明

| 参数序号 | 参数类型 | 参数说明             | 备注                                                         |
| ---- | ---- | ---- | ---- |
| 1 | 整型 | TabLayout控件的父窗口句柄 | 调用InitInstallPage或InitUnInstallPage接口返回的句柄。 |
| 2 | 字符串 | TabLayout控件的名称 | 对应到xml文件内TabLayout控件的name属性。 |
| 3 | 整型 | Tab页的序号 | 第一个Tab页的序号为0 |

###### 返回值

无

##### 5. ShowPage

> 显示安装或卸载界面

###### 调用示例

` nsDuiSkin::ShowPage `

###### 参数说明

该接口没有参数

###### 返回值

无

##### 6. SelectInstallDir

> 显示选择安装路径对话框

###### 调用示例

` nsDuiSkin::SelectInstallDir $hInstallDlg "请选择安装路径" `

###### 参数说明

| 参数序号 | 参数类型 | 参数说明             | 备注                                                         |
| ---- | ---- | ---- | ---- |
| 1 | 整型 | 安装程序窗口句柄 | 调用InitInstallPage或InitUnInstallPage接口返回的句柄。 |
| 2 | 字符串 | 文件夹选择框的标题 |      |

###### 返回值

返回选择的安装路径，取消选择时返回空字符串。

##### 7. InitMessageBox

> 初始化弹出提示框

###### 调用示例

` nsDuiSkin::InitMessageBox "msgbox.xml" "btnOK" "btnCancel,btnClose" `

###### 参数说明

| 参数序号 | 参数类型 | 参数说明                                     | 备注                                                         |
| -------- | -------- | -------------------------------------------- | ------------------------------------------------------------ |
| 1        | 字符串   | 弹出提示框使用的UI配置文件（xml）            |                                                              |
| 2        | 字符串   | 指定弹出窗口退出时，返回IDOK的按钮的名称     | 如果使用BindCallBack绑定了此按钮的回调函数，则此设置不生效。 |
| 3        | 字符串   | 指定弹出窗口退出时，返回IDCANCEL的按钮的名称 | 如果使用BindCallBack绑定了此按钮的回调函数，则此设置不生效。 |

###### 返回值

返回弹出提示框的窗口句柄

##### 8. ShowMessageBox

> 显示弹出对话框

###### 调用示例

` nsDuiSkin::ShowMessageBox `

###### 参数说明

此接口没有参数

###### 返回值

点击确定按钮返回“1”，点击取消按钮返回“0”。

##### 9. SetControlAttribute

> 设置控件属性值。此接口用于设置窗口界面上指定元素的属性值，例如：是否可见、是否选中、文字、背景色、背景图片等。

###### 调用示例

` nsDuiSkin::SetControlAttribute $hInstallDlg "btnInstall" "visible" "true" `

###### 参数说明

| 参数序号 | 参数类型 | 参数说明             | 备注                                                         |
| ---- | ---- | ---- | ---- |
| 1 | 整型 | 要设置属性值的元素所在窗口的句柄 | 调用InitInstallPage、InitUnInstallPage以及InitMessageBox接口时返回的句柄。 |
| 2 | 字符串 | 控件的名称 | 对应xml文件中控件的name属性 |
| 3 | 字符串 | 控件的属性名 |      |
| 4 | 字符串 | 控件的属性值 |      |

###### 返回值

无

##### 10. GetControlAttribute

> 获取控件属性值

###### 调用示例

` nsDuiSkin::GetControlAttribute $hInstallDlg "editDir" "text" `

###### 参数说明

| 参数序号 | 参数类型 | 参数说明             | 备注                                                         |
| ---- | ---- | ---- | ---- |
| 1 | 整型 | 要获取属性值的控件所在的窗口句柄 | 调用InitInstallPage、InitUnInstallPage以及InitMessageBox接口时返回的句柄。 |
| 2 | 字符串 | 控件的名称 | 对应xml文件中控件的name属性 |
| 3 | 字符串 | 控件的属性名 |      |

###### 返回值

返回指定控件的属性值。若控件或对应的属性值没有找到则返回空字符串。

###### 注意

**目前仅支持text、selected属性值的获取。**

##### 11. BindCallBack

> 绑定控件相关事件

###### 调用示例

```
#开始安装按钮事件
GetFunctionAddress $0 OnBtnInstallClick
nsDuiSkin::BindCallBack $hInstallDlg "btnInstall" $0
```
```
#绑定路径变化的通知事件
GetFunctionAddress $0 OnEditDirTextChange
nsDuiSkin::BindCallBack $hInstallDlg "editDir" $0
```

###### 参数说明

| 参数序号 | 参数类型 | 参数说明             | 备注                                                         |
| ---- | ---- | ---- | ---- |
| 1 | 整型 | 要绑定事件的控件所在窗口的句柄 | 调用InitInstallPage、InitUnInstallPage以及InitMessageBox接口时返回的句柄。 |
| 2 | 字符串 | 要绑定事件的控件的名称 | 对应xml文件中控件的name属性 |
| 3 | 整型 | 要绑定的NSIS函数的地址 | 使用NSIS提供的GetFunctionAddress函数获取地址 |

###### 返回值

无

###### 其他说明

1. 当绑定的控件是RichEdit时，其中文本内容变化时会触发绑定的函数。

2. 为了能够将通过Alt+F4及在任务栏使用右键菜单关闭窗口的事件通知给NSIS，绑定控件事件时需要绑定一个特殊的名称，这个名称是：syscommandclose。具体代码如下所示：
   ```
   #绑定窗口通过alt+f4等方式关闭时的通知事件 
   GetFunctionAddress $0 OnSysCommandCloseEvent
   nsDuiSkin::BindCallBack $hInstallDlg "syscommandclose" $0
   ```

##### 12. ExitDUISetup

> 结束安装（退出安装过程或卸载过程)

###### 调用示例

` nsDuiSkin::ExitDUISetup `

###### 参数说明

此接口没有参数

###### 返回值

无