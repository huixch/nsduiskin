#include<Windows.h>
#include<tchar.h>
#include "nsis.h"

extra_parameters *g_extraparameters = NULL;
unsigned int g_stringsize = 0;
stack_t **g_stacktop = NULL;
TCHAR *g_variables = NULL;

void NSISCALL PushString(LPTSTR str)
{
	stack_t *st;
	if (!g_stacktop) return;
	st = (stack_t*)GlobalAlloc(GPTR, sizeof(stack_t) + g_stringsize);
	lstrcpyn(st->text, str, g_stringsize);
	st->next = *g_stacktop;
	*g_stacktop = st;
}

int NSISCALL PopString(LPTSTR str)
{
	stack_t *st;
	if (!g_stacktop || !*g_stacktop) return 1;
	st = (*g_stacktop);
	//lstrcpyn(str, st->text, lstrlen(st->text) + 1);
	lstrcpy(str, st->text);
	*g_stacktop = st->next;
	GlobalFree(st);
	return 0;
}

void NSISCALL PushInt(long value)
{
	wchar_t buf[512] = { 0 };
	swprintf_s(buf, _T("%ld"), value);
	PushString(buf);
}

int NSISCALL PopInt()
{
	wchar_t buf[512] = { 0 };
	PopString(buf);
	return _wtoi(buf);
}

LPTSTR  NSISCALL GetUserVariable(int varnum)
{
	if (varnum < 0 || varnum >= __INST_LAST) return NULL;
	return g_variables + varnum * g_stringsize;
}

void NSISCALL SetUserVariable(int varnum, LPTSTR var)
{
	if (var != NULL && varnum >= 0 && varnum < __INST_LAST)
	{
		lstrcpy(g_variables + varnum * g_stringsize, var);
	}
}