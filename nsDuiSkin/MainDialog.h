#include "UIlib.h"
#include <map>
#include "MsgDialog.h"

using namespace DuiLib;

class CMainDialog : public WindowImplBase
{
private:
	int m_RunMode;
	CDuiString m_LicenseFile;
	std::map<CDuiString, int> m_ControllCallbackMap;
	CMsgDialog *m_MsgDialog;
	bool m_InitState;
protected:
	virtual CDuiString GetSkinFile();
	virtual LPCTSTR GetWindowClassName(void) const { return _T("CMainDialog"); };
public:
	CMainDialog();
	~CMainDialog();
	void SetMode(int mode);
	void SetLicenceFile(LPCTSTR filename);
	virtual void Notify(TNotifyUI& msg);
	void AddControlCallback(LPCTSTR controlname, int callback);
	HWND InitMessageBox(CDuiString skinfile, LPCTSTR btnOk, LPCTSTR btnCancel);
	UINT ShowMessageBox();
	void SetControlAttribute(HWND hwnd, LPCTSTR name, LPCTSTR attr, LPCTSTR value);
	CDuiString GetControlAttribute(HWND hwnd, LPCTSTR name, LPCTSTR attr);
	virtual LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	void ShowTabPageItem(LPCTSTR name, int index);
	void SetInitState(bool bInit);
	bool GetInitState();
};