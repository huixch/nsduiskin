#include "stdafx.h"
#include "MainDialog.h"
#include "nsis.h"
#include "MsgDialog.h"


CMainDialog::CMainDialog()
	:m_InitState(false),
	m_RunMode(1)
{

}

CMainDialog::~CMainDialog()
{

}

void CMainDialog::SetMode(int mode)
{
	m_RunMode = mode;
}

static DWORD CALLBACK EditStreamCallback(DWORD_PTR dwCookie, LPBYTE lpBuff, LONG cb, PLONG pcb)
{
	HANDLE hFile = (HANDLE)dwCookie;
	if (ReadFile(hFile, lpBuff, cb, (DWORD *)pcb, NULL))
	{
		return 0;
	}
	return -1;
}

void CMainDialog::SetLicenceFile(LPCTSTR filename)
{
	m_LicenseFile = filename;
	CDuiString path = this->m_pm.GetResourcePath();
	if (path.IsEmpty()) return;
	if (path.Right(1) != _T("\\"))
	{
		path += _T("\\");
	}
	CDuiString fullname = path + m_LicenseFile;
	fullname.MakeLower();
	CDuiString ext = fullname.Right(4);
	HANDLE hFile = CreateFile(filename, GENERIC_READ,
							  FILE_SHARE_READ, 0, OPEN_EXISTING,
							  FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if (hFile == INVALID_HANDLE_VALUE) return;
	EDITSTREAM es = { 0 };
	es.dwCookie = (DWORD_PTR)hFile;
	es.pfnCallback = EditStreamCallback;
	HRESULT hr;
	CRichEditUI *edit = static_cast<CRichEditUI *>(this->m_pm.FindControl(_T("editLicense")));
	if (edit == NULL) return;

	if (ext == _T(".txt")) // txt文件
	{
		hr = edit->StreamIn(SF_TEXT, es);
	}
	else if (ext == _T(".rtf")) // rtf文件
	{
		hr = edit->StreamIn(SF_RTF, es);
	}
	CloseHandle(hFile);
}

CDuiString CMainDialog::GetSkinFile()
{
	return m_RunMode == 1 ? _T("install.xml") : _T("uninstall.xml");
}

void CMainDialog::Notify(TNotifyUI& msg)
{
	std::map<CDuiString, int>::iterator iter = m_ControllCallbackMap.find(msg.pSender->GetName());
	if (_tcsicmp(msg.sType, _T("click")) == 0)
	{
		if (iter != m_ControllCallbackMap.end())
		{
			g_extraparameters->ExecuteCodeSegment(iter->second - 1, 0);
			return;
		}
	}

	if (wcscmp(msg.sType, _T("textchanged")) == 0)
	{
		if (iter != m_ControllCallbackMap.end())
		{
			g_extraparameters->ExecuteCodeSegment(iter->second - 1, 0);
			return;
		}
	}
	WindowImplBase::Notify(msg);
}

void CMainDialog::AddControlCallback(LPCTSTR controlname, int callback)
{
	m_ControllCallbackMap[controlname] = callback;
}

HWND CMainDialog::InitMessageBox(CDuiString skinfile, LPCTSTR btnOk, LPCTSTR btnCancel)
{
	m_MsgDialog = new CMsgDialog();
	m_MsgDialog->Create(this->m_hWnd, _T("提示"), skinfile, btnOk, btnCancel);
	m_MsgDialog->CenterWindow();
	return m_MsgDialog->GetHWND();
}

UINT CMainDialog::ShowMessageBox()
{
	UINT ret = m_MsgDialog->ShowModal();
	m_MsgDialog = NULL;
	return ret;
}

void CMainDialog::SetControlAttribute(HWND hwnd, LPCTSTR name, LPCTSTR attr, LPCTSTR value)
{
	if (this->GetHWND() == hwnd)
	{
		CControlUI *ctrl = this->m_pm.FindControl(name);
		if (ctrl != NULL)
		{
			ctrl->SetAttribute(attr, value);
		}
	}
	else {
		if (m_MsgDialog != NULL)
		{
			m_MsgDialog->SetControlAttribute(hwnd, name, attr, value);
		}		
	}
}

CDuiString CMainDialog::GetControlAttribute(HWND hwnd, LPCTSTR name, LPCTSTR attr)
{
	CDuiString sText(_T(""));
	if (this->GetHWND() == hwnd)
	{		
		CControlUI *ctrl = this->m_pm.FindControl(name);
		if (ctrl != NULL)
		{
			if (_tcsicmp(attr, _T("text")) == 0)
			{
				sText = ctrl->GetText();
			}
			else if (_tcsicmp(attr, _T("visible")) == 0)
			{
				sText = ctrl->IsVisible() ? _T("1") : _T("0");
			}
			else if (_tcsicmp(attr, _T("selected")) == 0)
			{
				CCheckBoxUI *chk = static_cast<CCheckBoxUI*>(ctrl);
				if (chk != NULL)
				{
					sText = chk->IsSelected() ? _T("1") : _T("0");
				}
			}			
		}
	}
	else
	{
		if (m_MsgDialog != NULL)
		{
			sText = m_MsgDialog->GetControlAttribute(hwnd, name, attr);
		}
	}
	return sText;
}

LRESULT CMainDialog::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL & bHandled)
{
	if (wParam == SC_CLOSE)
	{
		std::map<CDuiString, int>::iterator iter = m_ControllCallbackMap.find(_T("syscommandclose"));
		if (iter != m_ControllCallbackMap.end())
		{
			g_extraparameters->ExecuteCodeSegment(iter->second - 1, 0);
			bHandled = TRUE;
			return 0;
		}
		else {
			return WindowImplBase::OnSysCommand(uMsg, wParam, lParam, bHandled);
		}
	}
	else
	{
		return WindowImplBase::OnSysCommand(uMsg, wParam, lParam, bHandled);
	}
}

void CMainDialog::ShowTabPageItem(LPCTSTR name, int index)
{
	CTabLayoutUI *tab = static_cast<CTabLayoutUI *>(this->m_pm.FindControl(name));
	if (tab != NULL)
	{
		tab->SelectItem(index);
	}
}

void CMainDialog::SetInitState(bool bInit)
{
	m_InitState = bInit;
}

bool CMainDialog::GetInitState()
{
	return m_InitState;
}

