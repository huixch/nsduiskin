#pragma once
#include "UIlib.h"
#include <map>

using namespace DuiLib;


class CMsgDialog : public WindowImplBase
{
private:
	CDuiString m_SkinFileName;
	CDuiString m_BtnOk;
	CDuiString m_BtnCancel;

	std::map<CDuiString, int> m_ControllCallbackMap;
protected:
	virtual CDuiString GetSkinFile();
	virtual LPCTSTR GetWindowClassName(void) const { return _T("CMsgDialog"); };
public:
	HWND Create(HWND hwndParent, LPCTSTR pstrName, LPCTSTR skinfile, LPCTSTR btnOk, LPCTSTR btnCancel);
	void AddControlCallback(LPTSTR controlname, int callback);
	void SetControlAttribute(HWND hwnd, LPCTSTR name, LPCTSTR attr, LPCTSTR value);
	CDuiString GetControlAttribute(HWND hwnd, LPCTSTR name, LPCTSTR attr);
	virtual void Notify(TNotifyUI& msg);
	virtual void OnFinalMessage(HWND hWnd);
	virtual LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
};

