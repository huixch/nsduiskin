// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"

#include "nsis.h"
#include "MainDialog.h"

#pragma comment( lib, "Shlwapi.lib" )


HINSTANCE g_hInstance;
HWND g_hwndParent;
CMainDialog *g_dialog = NULL;

#define NSMETHOD_INIT(parent) {\
        g_hwndParent = parent; \
		g_extraparameters = extra; \
        EXDLL_INIT(); }

#define NSISAPI extern "C" __declspec(dllexport) void __cdecl 

BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	g_hInstance = (HINSTANCE)hModule;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

static UINT_PTR PluginCallback(enum NSPIM msg)
{
	return 0;
}

// 初始化安装程序自定义页面
NSISAPI InitInstallPage(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	CoInitialize(NULL);
	TCHAR resdir[MAX_PATH] = { 0 };
	TCHAR licencefile[MAX_PATH] = { 0 };
	extra->RegisterPluginCallback(g_hInstance, PluginCallback);
	PopString(resdir);
	PopString(licencefile);
	CPaintManagerUI::SetInstance(g_hInstance);
	CPaintManagerUI::SetResourcePath(resdir);
	CPaintManagerUI::SetResourceZip(_T("skin.zip"));
	g_dialog = new CMainDialog();
	g_dialog->SetMode(1);	
	g_dialog->Create(NULL, _T("XXXX安装程序"), WS_POPUP, 0, 0, 550, 410);
	g_dialog->CenterWindow();
	g_dialog->SetLicenceFile(licencefile);
	
	g_dialog->ShowWindow(false);
	PushInt((int)g_dialog->GetHWND());
}

NSISAPI InitUnInstallPage(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	CoInitialize(NULL);
	TCHAR resdir[MAX_PATH] = { 0 };
	extra->RegisterPluginCallback(g_hInstance, PluginCallback);
	PopString(resdir);
	CPaintManagerUI::SetInstance(g_hInstance);
	CPaintManagerUI::SetResourcePath(resdir);
	CPaintManagerUI::SetResourceZip(_T("skin.zip"));
	g_dialog = new CMainDialog();
	g_dialog->SetMode(2);
	g_dialog->Create(NULL, _T("XXXX卸载程序"), WS_POPUP, 0, 0, 550, 410);
	PushInt((int)g_dialog->GetHWND());
}

NSISAPI SetWindowTile(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	TCHAR title[1024] = { 0 };
	HWND hwnd = (HWND)PopInt();
	PopString(title);
	if (IsWindow(hwnd))
	{
		SetWindowText(hwnd, title);
		// 设置窗口图标
		CDuiString iconfile = CPaintManagerUI::GetResourcePath();

		if (iconfile.Right(1) == _T("\\"))
		{
			iconfile.Append(_T("logo.ico"));
		}
		else
		{
			iconfile.Append(_T("\\logo.ico"));
		}
		if (PathFileExists(iconfile))
		{
			HICON hIcon = (HICON)::LoadImage(NULL, iconfile, IMAGE_ICON,
				(::GetSystemMetrics(SM_CXICON) + 15) & ~15, (::GetSystemMetrics(SM_CYICON) + 15) & ~15,	// 防止高DPI下图标模糊
				LR_LOADFROMFILE);
			DWORD dwError = GetLastError();
			TCHAR buff[1024] = { 0 };
			_itow((int)dwError, buff, 16);
			ASSERT(hIcon);
			::SendMessage(hwnd, WM_SETICON, ICON_BIG, (LPARAM)hIcon);

			hIcon = (HICON)::LoadImage(NULL, iconfile, IMAGE_ICON,
				(::GetSystemMetrics(SM_CXICON) + 15) & ~15, (::GetSystemMetrics(SM_CYICON) + 15) & ~15,	// 防止高DPI下图标模糊
				LR_LOADFROMFILE);
			ASSERT(hIcon);
			::SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);
			hIcon = (HICON)::LoadImage(NULL, iconfile, IMAGE_ICON,
				(::GetSystemMetrics(SM_CXICON) + 15) & ~15, (::GetSystemMetrics(SM_CYICON) + 15) & ~15,	// 防止高DPI下图标模糊
				LR_LOADFROMFILE);
			ASSERT(hIcon);
			::SendMessage(hwnd, STM_SETICON, IMAGE_ICON, (LPARAM)hIcon);
		}
	}
}

NSISAPI ShowPage(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	if (g_dialog != NULL) {
		g_dialog->CenterWindow();
		g_dialog->ShowWindow(true);
		if (!g_dialog->GetInitState())
		{
			g_dialog->SetInitState(true);
			CPaintManagerUI::MessageLoop();
		}		
	}
}

NSISAPI ShowTabPageItem(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	HWND hwnd = (HWND)PopInt();
	TCHAR name[512] = { 0 };
	PopString(name);
	int index = PopInt();	
	if (g_dialog != NULL)
	{
		g_dialog->ShowTabPageItem(name, index);
	}
}

NSISAPI BindCallBack(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	int hwnd;
	TCHAR control[512] = { 0 };
	int callback;
	hwnd = PopInt();
	PopString(control);
	callback = PopInt();
	if ((HWND)hwnd == g_dialog->GetHWND())
	{
		g_dialog->AddControlCallback(control, callback);
	}
}

NSISAPI InitMessageBox(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	if (g_dialog != NULL)
	{
		TCHAR skinfile[MAX_PATH] = { 0 };
		TCHAR btnOK[512] = { 0 };
		TCHAR btnCancel[512] = { 0 };
		PopString(skinfile);
		PopString(btnOK);
		PopString(btnCancel);
		HWND hwnd = g_dialog->InitMessageBox(skinfile, btnOK, btnCancel);
		PushInt((int)hwnd);
	}
}

NSISAPI ShowMessageBox(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	if (g_dialog != NULL)
	{
		UINT ret = g_dialog->ShowMessageBox();
		PushInt((int)ret);
	}
}

NSISAPI SetControlAttribute(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	int hwnd = PopInt();
	TCHAR name[512] = { 0 };
	TCHAR attr[512] = { 0 };
	TCHAR value[1024] = { 0 };
	PopString(name);
	PopString(attr);
	PopString(value);
	if (g_dialog != NULL)
	{
		g_dialog->SetControlAttribute((HWND)hwnd, name, attr, value);
	}
}

NSISAPI GetControlAttribute(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	int hwnd = PopInt();
	TCHAR name[512] = { 0 };
	TCHAR attr[512] = { 0 };
	TCHAR value[1024] = { 0 };
	PopString(name);
	PopString(attr);
	if (g_dialog != NULL)
	{
		CDuiString cval = g_dialog->GetControlAttribute((HWND)hwnd, name, attr);
		lstrcpyn(value, cval, 1023);
		PushString(value);
	}
}

NSISAPI SetWindowSize(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	int hwnd = PopInt();
	int width = PopInt();
	int height = PopInt();
	if (width > -1 && height > -1)
	{
		g_dialog->ResizeClient(width, height);
	}	
}

NSISAPI SelectInstallDir(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	NSMETHOD_INIT(hwndParent);
	TCHAR title[512] = { 0 };
	int hwnd = PopInt();
	PopString(title);
	{
		BROWSEINFO bi;
		memset(&bi, 0, sizeof(BROWSEINFO));
		bi.hwndOwner = g_dialog->GetHWND();
		bi.lpszTitle = title;
		bi.ulFlags = 0x0040;

		TCHAR szFolderPath[MAX_PATH] = { 0 };
		LPITEMIDLIST idl = SHBrowseForFolder(&bi);
		if (idl == NULL) {
			PushString(szFolderPath);
			return;
		}

		SHGetPathFromIDList(idl, szFolderPath);
		PushString(szFolderPath);
	}
}

NSISAPI ExitDUISetup(HWND hwndParent, int string_size, TCHAR *variables, stack_t **stacktop, extra_parameters *extra)
{
	ExitProcess(0);
}
