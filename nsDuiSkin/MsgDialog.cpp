#include "stdafx.h"
#include "MsgDialog.h"
#include "nsis.h"


CDuiString CMsgDialog::GetSkinFile()
{
	return m_SkinFileName;
}

HWND CMsgDialog::Create(HWND hwndParent, LPCTSTR pstrName, LPCTSTR skinfile, LPCTSTR btnOk, LPCTSTR btnCancel)
{
	m_SkinFileName = skinfile;
	m_BtnOk = btnOk;
	m_BtnCancel = btnCancel;
	
	return WindowImplBase::Create(hwndParent, pstrName, WS_POPUPWINDOW | WS_CAPTION | WS_DLGFRAME | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0);
}

void CMsgDialog::AddControlCallback(LPTSTR controlname, int callback)
{
	m_ControllCallbackMap[controlname] = callback;
}

void CMsgDialog::SetControlAttribute(HWND hwnd, LPCTSTR name, LPCTSTR attr, LPCTSTR value)
{
	if (this->GetHWND() == hwnd)
	{
		CControlUI *ctrl = this->m_pm.FindControl(name);
		if (ctrl != NULL)
		{
			ctrl->SetAttribute(attr, value);
		}
	}
}

CDuiString CMsgDialog::GetControlAttribute(HWND hwnd, LPCTSTR name, LPCTSTR attr)
{
	CDuiString sText(_T(""));
	if (this->GetHWND() == hwnd)
	{
		CControlUI *ctrl = this->m_pm.FindControl(name);
		if (ctrl != NULL)
		{
			if (_tcsicmp(attr, _T("text")) == 0)
			{
				sText = ctrl->GetText();
			}
			else if (_tcsicmp(attr, _T("visible")) == 0)
			{
				sText = ctrl->IsVisible() ? _T("1") : _T("0");
			}
			else if (_tcsicmp(attr, _T("selected")) == 0)
			{
				CCheckBoxUI *chk = static_cast<CCheckBoxUI*>(ctrl);
				if (chk != NULL)
				{
					sText = chk->IsSelected() ? _T("1") : _T("0");
				}
			}
		}
	}
	return sText;
}

void CMsgDialog::Notify(TNotifyUI & msg)
{
	std::map<CDuiString, int>::iterator iter = m_ControllCallbackMap.find(msg.pSender->GetName());
	if (_tcsicmp(msg.sType, _T("click")) == 0)
	{
		// 如果已经给控件绑定了NSIS回调函数，则调用回调函数
		if (iter != m_ControllCallbackMap.end())
		{
			g_extraparameters->ExecuteCodeSegment(iter->second - 1, 0);
			return;
		}
		// 没有绑定事件时，确定按钮返回1
		if (m_BtnOk.Find(msg.pSender->GetName()) > -1)
		{
			Close(1);
			return;
		}
		// 没有绑定事件时，取消按钮返回0
		if (m_BtnCancel.Find(msg.pSender->GetName()) > -1) {
			Close(0);
			return;
		}
	}
	WindowImplBase::Notify(msg);
}

void CMsgDialog::OnFinalMessage(HWND hWnd)
{
	WindowImplBase::OnFinalMessage(hWnd);
	delete this;
}

LRESULT CMsgDialog::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL & bHandled)
{
	if (wParam == SC_CLOSE)
	{
		std::map<CDuiString, int>::iterator iter = m_ControllCallbackMap.find(_T("syscommandclose"));
		if (iter != m_ControllCallbackMap.end())
		{
			g_extraparameters->ExecuteCodeSegment(iter->second - 1, 0);
			bHandled = TRUE;
			return 0;
		}
		else {
			return WindowImplBase::OnSysCommand(uMsg, wParam, lParam, bHandled);
		}
	}
	else
	{
		return WindowImplBase::OnSysCommand(uMsg, wParam, lParam, bHandled);
	}
}
